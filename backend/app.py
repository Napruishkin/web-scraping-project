from typing import List, Optional
import requests
from bs4 import BeautifulSoup
from celery import Celery
from flask import Flask, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String
from flask_cors import CORS

from backend.config import Config

app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
db = SQLAlchemy(app)

# Create Celery instance
celery_app = Celery(
    app.name,
    backend=Config.result_backend,
    broker=Config.broker_url
)

celery_app.conf.update(app.config)
celery_app.conf.broker_connection_retry_on_startup = True


class OLXListing(db.Model):
    __tablename__ = 'olx_listings'

    id: int = Column(Integer, primary_key=True, index=True)
    title: str = Column(String)
    category: str = Column(String)
    location_date: str = Column(String)
    price: str = Column(String)

    def __repr__(self):
        return f'<Item: {self.title}>'


@app.route('/api/listings')
def get_listings() -> Response:
    listings: List[OLXListing] = OLXListing.query.all()

    json_listings = [
        {
            'id': listing.id,
            'title': listing.title,
            'category': listing.category,
            'location_date': listing.location_date,
            'price': listing.price
        }
        for listing in listings
    ]
    return jsonify(json_listings)


@app.route('/api/scrape/<category>/<sub_category>', methods=['GET'])
def scrape(category: str, sub_category: Optional[str] = None) -> jsonify:
    result = scrape_olx.apply_async(args=[category, sub_category])
    return jsonify({'message': f'Task started with id {result.id}'}), 202


@celery_app.task
def scrape_olx(category: str, sub_category: Optional[str] = None) -> List[dict]:
    base_url = "https://www.olx.pl"
    page_count = 4
    results = []

    for page in range(1, page_count + 1):
        url = f"{base_url}/{category}/?page={page}"

        if sub_category is not None:
            url = f"{base_url}/{category}/{sub_category}/?page={page}"
        response = requests.get(url)

        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')

            listings = soup.find_all('div', class_='css-1sw7q4x')

            for listing in listings:
                title_element = listing.find('h6', class_='css-16v5mdi er34gjf0')

                if title_element:
                    title = title_element.text.strip()
                else:
                    continue

                price_element = listing.find('p', {'data-testid': 'ad-price', 'class': 'css-10b0gli er34gjf0'})
                location_date_element = listing.find('p',
                                                     {'data-testid': 'location-date', 'class': 'css-veheph er34gjf0'})

                if price_element:
                    price = price_element.text.strip()
                else:
                    price = "Price not available"

                location_date = location_date_element.text.strip() if location_date_element else ("Location and date "
                                                                                                  "not available")

                olx_listing = OLXListing(title=title, category=category, location_date=location_date, price=price)
                with app.app_context():
                    db.session.add(olx_listing)
                    db.session.commit()

                results.append({'title': title, 'category': category})

        else:
            print(f"Failed to retrieve data from {url}. Status code: {response.status_code}")

    return results


if __name__ == '__main__':
    app.run(debug=True)
