import os


class Config:
    # Flask app secret key
    SECRET_KEY = os.environ.get('SECRET_KEY', 'your_secret_key')

    # SQLALCHEMY_DATABASE_URI = "postgresql://postgres:test123@localhost:5432/postgres"
    SQLALCHEMY_DATABASE_URI = ("postgresql://xolwyprtunjbgu"
                               ":fdf5e53c2927ea236b0c1812b02a7ed8ada4a5a92c1f76c580ce1d1d2b0a044b@ec2-44-213-228-107"
                               ".compute-1.amazonaws.com:5432/dd61s8vj83m6je")

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    result_backend = 'redis://127.0.0.1:6379/0'
    broker_url = 'redis://127.0.0.1:6379/0'
