# Web Scraping Project

This project demonstrates web scraping using Python (Flask) for the backend and React for the frontend. It includes an authentication example and a product listings page.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Backend](#backend)
- [Frontend](#frontend)
- [Usage](#usage)

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Python 3.x installed
- Node.js and npm installed

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/Napruishkin/web-scraping-project.git
   cd web-scraping-project
   ```

Install backend dependencies:

```bash
cd backend
python -m venv venv
source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
pip install -r requirements.txt
```

Install frontend dependencies:

```bash
cd frontend
npm install
```

## Backend
### Database Setup

Initialize the database:

```bash
flask db init
flask db migrate
flask db upgrade
```

### Run the Backend
Start the Flask app:

```bash
flask run
```

Access the API at http://localhost:5000.

## Frontend
### Run the Frontend

In the frontend directory:

```bash
npm start
```

Access the frontend at http://localhost:3000.

## Usage
Open your browser and navigate to http://localhost:3000.
Authenticate using the provided login button.
Explore the product listings page.

For add new item by category: navigate to http://localhost:5000/api/scrape/<category>/<sub_category> or http://localhost:5000/api/scrape/<category>
Check all db listing: http://localhost:5000/api/listings


