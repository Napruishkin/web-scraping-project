import React, { useState } from 'react';

const AuthorizationPage = ({ onAuthenticate }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [notification, setNotification] = useState('');
  const [showListings, setShowListings] = useState(false);

  const handleLogin = async () => {
    setIsLoading(true);

    setTimeout(() => {
      if (username === 'demoUser' && password === 'demoPassword') {
        setIsAuthenticated(true);
        setNotification('');
        setShowListings(true);
        onAuthenticate(true);
      } else {
        setNotification('Invalid credentials. Please try again.');
      }
      setIsLoading(false);
    }, 1000);
  };

  return (
    <div style={styles.container}>
      <div style={styles.card}>
        <h2 style={styles.title}>Authorization Page</h2>
        {notification && <p style={styles.notification}>{notification}</p>}
        {!isAuthenticated && !isLoading && (
          <div style={styles.form}>
            <label style={styles.label}>
              Username:
              <input
                style={styles.input}
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </label>
            <br />
            <label style={styles.label}>
              Password:
              <input
                style={styles.input}
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>
            <br />
            <button style={styles.button} onClick={handleLogin}>
              Login
            </button>
          </div>
        )}
        {isLoading && <p style={styles.loading}>Loading...</p>}
        {isAuthenticated && showListings && (
          <div>
            <p>Welcome, {username}!</p>
            {/* Additional content for authenticated users (listings, etc.) */}
          </div>
        )}
      </div>
    </div>
  );
};

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    backgroundColor: '#f2f2f2',
  },
  card: {
    width: '300px',
    padding: '20px',
    borderRadius: '8px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#ffffff',
  },
  title: {
    textAlign: 'center',
    fontSize: '24px',
    marginBottom: '20px',
    color: '#333333',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  label: {
    marginBottom: '8px',
    color: '#555555',
  },
  input: {
    padding: '8px',
    marginBottom: '16px',
    border: '1px solid #cccccc',
    borderRadius: '4px',
  },
  button: {
    padding: '10px',
    backgroundColor: '#3498db',
    color: '#ffffff',
    borderRadius: '4px',
    cursor: 'pointer',
  },
  notification: {
    textAlign: 'center',
    color: '#e74c3c',
    marginBottom: '10px',
  },
  loading: {
    textAlign: 'center',
    color: '#3498db',
    marginTop: '10px',
  },
};

export default AuthorizationPage;
