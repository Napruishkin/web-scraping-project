import React, { useState, useEffect } from 'react';

const ProductListings = () => {
  const [listings, setListings] = useState([]);

  useEffect(() => {
    fetch('/api/listings')
      .then(response => response.json())
      .then(data => setListings(data));
  }, []);

  const handleLogout = () => {
    // Clear the authentication state in local storage
    localStorage.removeItem('isAuthenticated');

    // Reload the page to reflect the logout
    window.location.reload();
  };

  return (
    <div style={styles.container}>
      <h2 style={styles.title}>Product Listings</h2>
      <button onClick={handleLogout} style={styles.logoutButton}>
        Logout
      </button>
      <ul style={styles.list}>
        {listings.map(listing => (
          <li key={listing.id} style={styles.listItem}>
            <strong>Title:</strong> {listing.title}<br />
            <strong>Category:</strong> {listing.category}<br />
            <strong>Location and Date:</strong> {listing.location_date}<br />
            <strong>Price:</strong> {listing.price}
          </li>
        ))}
      </ul>
    </div>
  );
};

const styles = {
  container: {
    maxWidth: '600px',
    margin: 'auto',
    padding: '20px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#ffffff',
  },
  title: {
    textAlign: 'center',
    fontSize: '24px',
    marginBottom: '20px',
    color: '#333333',
  },
  logoutButton: {
    backgroundColor: '#ff6347',
    color: '#ffffff',
    padding: '10px',
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
    marginTop: '10px',
  },
  list: {
    listStyle: 'none',
    padding: '0',
  },
  listItem: {
    padding: '10px',
    borderBottom: '1px solid #cccccc',
    fontSize: '16px',
    color: '#555555',
  },
};

export default ProductListings;
