import React, { useState, useEffect } from 'react';
import AuthorizationPage from './components/AuthorizationPage';
import ProductListings from './components/ProductListings';

const App = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  // Check if the user is authenticated on component mount
  useEffect(() => {
    const storedAuth = localStorage.getItem('isAuthenticated');
    if (storedAuth) {
      setIsAuthenticated(JSON.parse(storedAuth));
    }
  }, []);

  const handleAuthentication = (authenticated) => {
    // Update state
    setIsAuthenticated(authenticated);

    // Save to local storage
    localStorage.setItem('isAuthenticated', JSON.stringify(authenticated));
  };

  return (
    <div>
      {!isAuthenticated ? (
        <AuthorizationPage onAuthenticate={handleAuthentication} />
      ) : (
        <ProductListings />
      )}
    </div>
  );
};

export default App;
